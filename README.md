## What is this repository for? ##
* Quick summary
This is my implementation for converting CSV to nested JSON. The CSV is having a structure of Parent and several nested children. The code goes through a CSV file converts it into list, removes spaces, checks for the levels and uses groupby function. The code understands parent,any levels of children and converts it into its respective parent-child nested JSON.

## AWS ##
The code is integrated to AWS cloud. The code reads CSV file from S3 (Simple Storage Services) buckets. After the conversion of CSV to nested json , it writes it back to AWS cloud. Also, it checks whether the CSV has the desired format of Parent and Child levels. The bucket name is mdev.tech-test.test.csvtojsontransformer.<random_alpha_numeric> and cluster name is ecs-euw1-n-mytest-service-001.

## Terraform as IAC for AWS ##
We have used terraform (Infrastructe as code) to execute the code in AWS. All the variables which will be used to create amazon instances, roles etc. are saved in terraform.tfvars file. To execute, we have terraform.sh file which initiates (this downloads all the required plugins for AWS) and applies terrform file by providing terrafrom variables.

## Jenkins ##
Using Jenkin Job DSL, we have created multibranchPipelineJob named as 'tech-test-solution-v2' where we have provided branchsource detail of bitbucket repository and credentials. It will discover branch(es) on the repository and pull requests originating from a branch in the repository itself. The Jenkin file has two stages. First Unit test i.e. executing unit test of source code after installing all the pre-requisite python modules.In the deploy stage, it builds the infrastructure in AWS using terraform. Then it will build and push docker as per the docker image. The dockerfile has the version of OS, python version and executable python code. 


## How do I get set up? ##

Local: (Windows)
------------
When running the python implementation, open your IDE and create a virtual environment. 
```
Create a virtual environment in a folder. 
Copy all files and folder present in tasks folder to the virtual environment folder.
From the virtual environment folder run the below commands

pip install -r requirements.txt

In the IDE Edit Configuration pass command line argument as : 

--aws_access_key_id <access_key>  --aws_secret_access_key <secret_key>

And environment variables as:

xxwmm_env = <dev/sit/uat/cit/prd>
bucket = <name of the bucket in the aws account>

python src/main.py

```

To Test:
------------
Windows:
```
Create a virtual environment in a folder. 
Copy all files and folder present in tasks folder to the virtual environment folder.
From the virtual environment folder run the below commands

pip install -r test_requirements.txt
pytest --cov=src --cov-report=html tests\

The test report would be stored in the htmlcov\index.html
```

Linux:
```
virtualenv venv
source venv/bin/activate 
cd task
pip install -r test_requirements.txt
export PYTHONPATH=$PYTHONPATH:$PWD
pytest --cov=src --cov-report=html tests/

The test report would be stored in the htmlcov/index.html
```

Current Test Coverage
------------------------
```
--coverage: platform win32, python 3.7.0-final-0 --
Name                 Stmts   Miss  Cover
----------------------------------------
src\__init__.py          0      0   100%
src\config.py           29     22    24%
src\csvutil.py          10      0   100%
src\exception.py        32      2    94%
src\group.py            17      0   100%
src\main.py             48     14    71%
src\storageutil.py      34      2    94%
src\transformer.py      38      0   100%
src\validate.py          7      1    86%
----------------------------------------
TOTAL                  215     41    81%
```

##Sample Input##
```
Base URL,Level 1 - Name,Level 1 - ID,Level 1 - URL,Level 2 - Name,Level 2 - ID,Level 2 - URL,Level 3 - Name,Level 3 - ID,Level 3 - URL,Level 4 - Name,Level 4 - ID,Level 4 - URL
https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,,,,,,,,,
,,,,,,,,,
https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,FRESH,178969,https://groceries.morrisons.com/browse/178974/178969,,,,,,
,,,,,,,,,
https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,BAKERY & CAKES,178971,https://groceries.morrisons.com/browse/178974/178971,,,,,,
https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,BAKERY & CAKES,178971,https://groceries.morrisons.com/browse/178974/178971,BREAD & BREAD ROLLS,179023,https://groceries.morrisons.com/browse/178974/178971/179023,,,
https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,BAKERY & CAKES,178971,https://groceries.morrisons.com/browse/178974/178971,"CAKES, PIES & TARTS",179024,https://groceries.morrisons.com/browse/178974/178971/179024,,,
https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,BAKERY & CAKES,178971,https://groceries.morrisons.com/browse/178974/178971,CROISSANTS & BREAKFAST BAKERY,179025,https://groceries.morrisons.com/browse/178974/178971/179025,,,
https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,BAKERY & CAKES,178971,https://groceries.morrisons.com/browse/178974/178971,DESSERTS & PUDDINGS,179026,https://groceries.morrisons.com/browse/178974/178971/179026,,,
https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,BAKERY & CAKES,178971,https://groceries.morrisons.com/browse/178974/178971,"FRUITED BREAD, SCONES & HOT CROSS BUNS",179027,https://groceries.morrisons.com/browse/178974/178971/179027,,,
,,,,,,,,,
https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,BAKERY & CAKES,178971,https://groceries.morrisons.com/browse/178974/178971,BREAD & BREAD ROLLS,179023,https://groceries.morrisons.com/browse/178974/178971/179023,BREAD,179078,https://groceries.morrisons.com/browse/178974/178971/179023/179078
https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,BAKERY & CAKES,178971,https://groceries.morrisons.com/browse/178974/178971,"CAKES, PIES & TARTS",179024,https://groceries.morrisons.com/browse/178974/178971/179024,CAKES,179079,https://groceries.morrisons.com/browse/178974/178971/179024/179079
```

##Sample Output##
```
[
  {
    "label": "THE BEST",
    "id": "178974",
    "link": "https://groceries.morrisons.com/browse/178974",
    "children": [
      {
        "label": "FRESH",
        "id": "178969",
        "link": "https://groceries.morrisons.com/browse/178974/178969",
        "children": []
      },
      {
        "label": "BAKERY & CAKES",
        "id": "178971",
        "link": "https://groceries.morrisons.com/browse/178974/178971",
        "children": [
          {
            "label": "BREAD & BREAD ROLLS",
            "id": "179023",
            "link": "https://groceries.morrisons.com/browse/178974/178971/179023",
            "children": [
              {
                "label": "BREAD",
                "id": "179078",
                "link": "https://groceries.morrisons.com/browse/178974/178971/179023/179078",
                "children": []
              }
            ]
          },
          {
            "label": "CAKES, PIES & TARTS",
            "id": "179024",
            "link": "https://groceries.morrisons.com/browse/178974/178971/179024",
            "children": [
              {
                "label": "CAKES",
                "id": "179079",
                "link": "https://groceries.morrisons.com/browse/178974/178971/179024/179079",
                "children": []
              }
            ]
          },
          {
            "label": "CROISSANTS & BREAKFAST BAKERY",
            "id": "179025",
            "link": "https://groceries.morrisons.com/browse/178974/178971/179025",
            "children": []
          },
          {
            "label": "DESSERTS & PUDDINGS",
            "id": "179026",
            "link": "https://groceries.morrisons.com/browse/178974/178971/179026",
            "children": []
          },
          {
            "label": "FRUITED BREAD, SCONES & HOT CROSS BUNS",
            "id": "179027",
            "link": "https://groceries.morrisons.com/browse/178974/178971/179027",
            "children": []
          },
          {
            "label": "BREAD & BREAD ROLLS",
            "id": "179023",
            "link": "https://groceries.morrisons.com/browse/178974/178971/179023",
            "children": [
              {
                "label": "BREAD",
                "id": "179078",
                "link": "https://groceries.morrisons.com/browse/178974/178971/179023/179078",
                "children": []
              }
            ]
          },
          {
            "label": "CAKES, PIES & TARTS",
            "id": "179024",
            "link": "https://groceries.morrisons.com/browse/178974/178971/179024",
            "children": [
              {
                "label": "CAKES",
                "id": "179079",
                "link": "https://groceries.morrisons.com/browse/178974/178971/179024/179079",
                "children": []
              }
            ]
          }
        ]
      }
    ]
  }
]

```