multibranchPipelineJob('tech-test-solution-v2') {
	branchSources {
		branchSource {
			source {
				bitbucket {
                    repoOwner('rohitbiswaslit93')
                    repository('tech-test-solution-v2')
                    credentialsId('Ricky@bitbucket!23')
				}
			}
		}
	}

      orphanedItemStrategy {
        discardOldItems {
            numToKeep(5)
            daysToKeep(3)
        }
      }

      triggers {
            periodic(10)
      }


	configure {
	  def traits = it / sources / data / 'jenkins.branch.BranchSource' / source / traits
	  traits << 'com.cloudbees.jenkins.plugins.bitbucket.BranchDiscoveryTrait' {
	    strategyId(3)
	  }
	}


	 configure {
	   def traits = it / sources / data / 'jenkins.branch.BranchSource' / source / traits
	   traits << 'com.cloudbees.jenkins.plugins.bitbucket.OriginPullRequestDiscoveryTrait' {
	     strategyId(3)
	    }
	 }


	configure {
	   def traits = it / sources / data / 'jenkins.branch.BranchSource' / source / traits
	   traits << 'com.cloudbees.jenkins.plugins.bitbucket.ForkPullRequestDiscoveryTrait' {
	     strategyId(3)
	    }
	 }

	configure {
	   def traits = it / sources / data / 'jenkins.branch.BranchSource' / source / traits
	   traits << 'jenkins.scm.impl.trait.RegexSCMHeadFilterTrait' {
	     regex('develop|release-.*|dev-.*|sit-.*|uat-.*|pre-.*|prd-.*')
	  }
	}


	 configure {
		def traits = it / sources / data / 'jenkins.branch.BranchSource' / source / traits
		traits << 'com.cloudbees.jenkins.plugins.bitbucket.TagDiscoveryTrait' {
	      strategyId(3)
		}
	}
}