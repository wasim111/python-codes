
provider "aws" {
  region = "${var.region}"
  assume_role {
  }
}

terraform {
  backend "s3" {
    key = "terraform.tfstate"
    region = "eu-west-1"
    encrypt = true
  }
}

resource "aws_ecr_repository" "test-transformer" {
  name = "${var.domain}/${var.serviceName}"
}
