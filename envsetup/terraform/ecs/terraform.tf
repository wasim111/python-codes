provider "aws" {
  region = "${var.region}"
  assume_role {
  }
}

terraform {
  backend "s3" {
    key = "terraform.tfstate"
    region = "eu-west-1"
    encrypt = true
  }
}

resource "random_string" "main" {
  length  = 10
  lower   = true
  upper   = false
  number  = false
  special = false
}

resource "aws_s3_bucket" "s3bucket" {
  bucket = "m${var.threeCharEnvDescription}.${var.project}.${var.domain}.${var.serviceName}.${random_string.main.result}"
  acl    = "private"
}

variable "logs_retention_in_days" {
  type        = number
  default     = 90
  description = "90 days of log retention"
}

resource "aws_cloudwatch_log_group" "cloudwatch-log-group" {
  name = "clg-${var.shortRegionDescription}-${var.threeCharEnvDescription}-${var.domain}-${var.project}-ecs-${var.threeDigitSeqId}"
  retention_in_days = var.logs_retention_in_days

  tags = {
    Environment = "${var.threeCharEnvDescription}"
    owner = "${var.domain}"
	region-id = "${var.shortRegionDescription}"
  }
}

resource "aws_ecs_task_definition" "task" {
  family             = "${var.serviceName}-${var.shortServiceName}-${var.threeCharEnvDescription}"
  task_role_arn      = "${var.testTaskRole}"

  container_definitions = <<EOF
[{
	"name": "${var.serviceName}-${var.shortServiceName}-${var.threeCharEnvDescription}",
	"image": "${var.account_id}.dkr.ecr.eu-west-1.amazonaws.com/${var.domain}/${var.serviceName}:${var.threeCharEnvDescription}",
	"cpu": ${var.taskCpu},
	"memory": ${var.taskMemory},
	"memoryReservation": ${var.taskMemoryReservation},
    "privileged": true,
	"portMappings": [{
		"containerPort": ${var.containerPort},
		"hostPort": 0,
		"protocol": "tcp"
	}],
	"essential": true,
	"mountPoints": [],
    "environment": [
      { "name": "xxwmm_env", "value": "${var.threeCharEnvDescription}" },
	  {	"name": "bucket", "value": "${aws_s3_bucket.s3bucket.bucket}" }
    ],
	"logConfiguration": {
		"logDriver": "awslogs",
		"options": {
			"awslogs-group": "clg-${var.shortRegionDescription}-${var.threeCharEnvDescription}-${var.domain}-${var.project}-ecs-${var.threeDigitSeqId}",
			"awslogs-region": "${var.region}",
			"awslogs-stream-prefix": "${var.serviceName}-${var.shortServiceName}-${var.threeCharEnvDescription}"
		}
	}
}]
EOF

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [eu-west-1a, eu-west-1b]"
  }
}

resource "aws_cloudwatch_event_rule" "CWEventRule" {
  name                = "TestTransformer-${var.threeCharEnvDescription}"
  description         = "Trigger ECS task"
  schedule_expression = "${var.eventCron}"
  is_enabled          = "${var.eventEnabled}"
}

resource "aws_cloudwatch_event_target" "scheduled_task" {
  rule = "${aws_cloudwatch_event_rule.CWEventRule.name}"
  target_id = "TestTransformer"
  arn = "arn:aws:ecs:${var.region}:${var.account_id}:cluster/${var.cluster}"
  role_arn = "${var.testTaskRole}"

  ecs_target {
    task_count = 1
    task_definition_arn = "${aws_ecs_task_definition.task.arn}"
    launch_type = "EC2"
  }
}
