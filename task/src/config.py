"""
config module. This module reads the config file using configparser
and pass variable data to each module
"""
import configparser
import os
import boto3
import argparse


def s3_client_session():
    """
    AWS S3 Boto3 client
    :return: s3 session client
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--aws_access_key_id')
    parser.add_argument('--aws_secret_access_key')
    args = parser.parse_args()
    aws_access_key_id = args.aws_access_key_id
    aws_secret_access_key = args.aws_secret_access_key
    if aws_access_key_id and aws_secret_access_key:
        session = boto3.session.Session(
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
        )
    else:
        session = boto3.session.Session()
    return session.client('s3')


class Config:
    """
    Config class to pass variables with respect to each environment
    """
    def __init__(self):
        """
        Init method of Config class
        """
        cfg = configparser.ConfigParser()
        if os.environ['xxwmm_env'] == 'dev':
            if os.name == 'nt':
                cfg.read(os.path.join(os.getcwd().replace('src', 'config'), 'property_dev.ini'))
            else:
                cfg.read('./config/property_dev.ini')


        self.s3bucket = os.environ['bucket'].strip()
        self.sourcefolder = cfg.get('s3_details', 'sourcefolder')
        self.targetfolder = cfg.get('s3_details', 'targetfolder')
        self.errorfolder = cfg.get('s3_details', 'errorfolder')
        self.archivefolder = cfg.get('s3_details', 'archivefolder')
        self.infileextension = cfg.get('s3_details', 'infileextension')
        self.outfileextension = cfg.get('s3_details', 'outfileextension')
