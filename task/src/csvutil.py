"""
csvutil module. This module has a class CsvReader whose
method reads the csv file line and removes all blank lines
from start and end whitespace characters for each field
"""
import csv


class CsvReader:
    """
    CSV Reader class. This class method reads the CSV file,
    removes blanl lines and whitespaces for each column
    """

    def __init__(self, lines):
        """
        Init Method of CSVReader
        :param lines: list of lines in the file
        """
        self.lines = lines

    def read_csv(self):
        """
        This methods reads the csv file, removes all blank lines from starting and
        ending whitespaces of each columns
        :return line_list: List containing all valid rows after removing blank lines
        """
        csv_reader = csv.reader(self.lines, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL,
                                skipinitialspace=True)
        next(csv_reader)
        lines_list = [list(map(str.strip, rowdata))
                      for rowdata in csv_reader if any(field.strip() for field in rowdata)]
        self.lines = list()
        return lines_list
