"""
group module. This module has a class GroupByLevels
whose method groups colums based on the levels and identify the
unique groups for each level
"""
from itertools import groupby


class GroupByLevels:
    """
    GroupByLevels class, this class method returns the unique groups
    based on the columns considered for each level
    """

    def __init__(self, level: int):
        """
        Init method for the class GroupByLevels
        :param level: Number of levels in the file
        """
        self.level = level

    def fields_in_group(self) -> str:
        """
        This menthod creates a string of the columns of the file
        which will be used in each levels to identify unique values in eaach level.
        :return: a string of the columns to be considered to group each level.
        """
        fields = 'r[0]'
        for i in range(1, self.level * 3 + 1):
            fields += f', r[{(i)}]'
        return fields

    def compare_expression(self) -> str:
        """
        This method creates a string the columns which will be used for comparision
        at each level with its child level to identify the childrens
        :return: a string of the columns with comparision operator used for comparision
        """
        expression = f'row1[1] == row2[1]'
        for i in range(2, self.level * 3 + 1):
            expression += f' and  row1[{i}] == row2[{i}]'
        return expression

    def unique_rows(self, lines_list: list) -> list:
        """
        This function use itertools groupby method to identify the unique values
        based on the field combination
        for each level. The unique values are stored in a list.
        :param lines_list: all rows of the file in the form of a list
        :return unique_level1: return the unique values in the form of list for a level
        """
        unique_rows_list = [group_key
                            for group_key, iter_data in
                            groupby(lines_list, lambda r: (eval(self.fields_in_group())))
                            if group_key[(self.level - 1) * 3 + 1]
                            or group_key[(self.level - 1) * 3 + 2]
                            or group_key[(self.level - 1) * 3 + 3]]
        return unique_rows_list
