"""
validate the csv file module
"""
from src.exception import ValidationError


def calculate_levels(lines: list) -> int:
    """
    This method return calculates the number of levels in the
    csv file. If there is invalid number of columns it raises a
    Validation error and rejects the file
    Valid number of column is level * 3 + 1
    :param lines: Lines of the file
    :return number_of_levels: number of levels in the file
    """
    number_of_elements = len(lines[0])
    if (number_of_elements - 1) % 3 == 0:
        number_of_levels = number_of_elements // 3
    else:
        raise ValidationError('The file has some extra or missing level elements')
    return number_of_levels
