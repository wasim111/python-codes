"""
transformer module. This module has a class Transformer
whose function does transformation from csv to nested json
"""
import json

from src.group import GroupByLevels


class Transformer:
    """
    Transformer class
    """

    def __init__(self, level: int):
        """
        Init method of Transformer class
        :param level: Number of levels in the file
        """
        self.level = level

    def map_children(self, dict_rows: dict) -> dict:
        """
        This method identify the childrens of each element
        :param dict_rows: A dictionary of list of unique elements at each level
        :return dict_children: A dictionary which maps each children to its parent element
        for each level.
        """
        dict_children = dict()
        for level_index in range(1, self.level):
            dict_children[f'unique{level_index}'] = dict()
            for indexj, row1 in enumerate(dict_rows[f'unique{level_index}']):
                dict_children[f'unique{level_index}'][indexj] = list()
                for indexk, row2 in enumerate(dict_rows[f'unique{level_index + 1}']):
                    if eval(GroupByLevels(level_index).compare_expression()):
                        dict_children[f'unique{level_index}'][indexj].append(indexk)
        return dict_children

    def transform_to_json(self, dict_rows: dict, children: dict) -> json:
        """
        This method transforms the rows provided in the input file into json
        :param dict_rows: A dictionary of list of unique elements at each level
        :param children: A dictionary which maps each children to its parent element
        for each level.
        :return: The r=transformed json object
        """
        for level_index in range(self.level - 1, 0, -1):
            for key, value in children[f'unique{level_index}'].items():
                for index, item in enumerate(value):
                    length = len(dict_rows[f'unique{level_index + 1}'][item])
                    group_dict = dict()
                    group_dict['label'] = dict_rows[f'unique{level_index + 1}'][item][length - 3]
                    group_dict['id'] = dict_rows[f'unique{level_index + 1}'][item][length - 2]
                    group_dict['link'] = dict_rows[f'unique{level_index + 1}'][item][length - 1]
                    if level_index == self.level - 1:
                        group_dict['children'] = list()
                    else:
                        group_dict['children'] = children[f'unique{level_index + 1}'][item]
                    children[f'unique{level_index}'][key][index] = group_dict

        output = list()
        for key in children['unique1']:
            group_dict = dict()
            length = len(dict_rows['unique1'][key])
            group_dict['label'] = dict_rows['unique1'][key][length - 3]
            group_dict['id'] = dict_rows['unique1'][key][length - 2]
            group_dict['link'] = dict_rows['unique1'][key][length - 1]
            group_dict['children'] = children['unique1'][key]
            output.append(group_dict)

        return json.dumps(output, indent=2)
