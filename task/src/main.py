"""
main module. This has the starting point of the program
"""
import traceback
import logging
from datetime import datetime

from src.transformer import Transformer
from src.group import GroupByLevels
from src.storageutil import S3
from src.csvutil import CsvReader
from src.config import Config, s3_client_session
from src.validate import calculate_levels
from src.exception import S3Error, ValidationError, FileNotExist, error_message


def main(config, s3_client):
    """
    Main method of the program
    :param config: object of the config class to pass the variables
    :param s3_client: AWS s3 client object
    :return:
    """
    try:
        s3 = S3(s3_client, config.s3bucket)
        file_list = s3.list_object(config.sourcefolder, config.infileextension)
        if not file_list:
            raise FileNotExist(f'No file exist in {config.s3bucket} '
                               f'and prefix {config.sourcefolder} '
                               f'for file extension {config.infileextension}')
        for file in file_list:
            try:
                lines = s3.read(file)
                lines_list = CsvReader(lines).read_csv()
                number_of_levels = calculate_levels(lines_list)
                unique_dict = dict()
                for level in range(1, number_of_levels + 1):
                    unique_dict['unique' + f'{level}'] = GroupByLevels(level).unique_rows(lines_list)
                transform = Transformer(level)
                children_dict = transform.map_children(unique_dict)
                json_output = transform.transform_to_json(unique_dict, children_dict)
            except Exception as ex:
                s3.move(file, file.replace(
                    config.sourcefolder, config.errorfolder) + datetime.now().strftime('%Y%m%d%H%M%S'))
                raise ValidationError(f'File {file} content is '
                                      f'not in proper format.f"Error: {str(ex)}')
            s3.write(json_output,
                     config.targetfolder + file.replace(config.sourcefolder, '').replace(
                         config.infileextension, config.outfileextension))
            s3.move(file, file.replace(config.sourcefolder, config.archivefolder)
                    + datetime.now().strftime('%Y%m%d%H%M%S'))
    except S3Error as ex:
        logging.error(error_message(ex.code, ex.message, ex.additionalmessage))
        traceback.print_exc()
    except FileNotExist as ex:
        logging.error(error_message(ex.code, ex.message, ex.additionalmessage))
        traceback.print_exc()
    except ValidationError as ex:
        logging.error(error_message(ex.code, ex.message, ex.additionalmessage))
        traceback.print_exc()
    except Exception as ex:
        logging.error(error_message('SE0000', 'Internal Error', f"Error: {str(ex)}"))
        traceback.print_exc()


if __name__ == '__main__':
    config_obj = Config()
    s3_session_client = s3_client_session()
    main(config_obj, s3_session_client)
