"""
s3util module. This module has class S3 whose method does all
s3 operation like list, read, write, move operation in AWS S3
"""
import logging

from src.exception import S3Error


class S3:
    """
    S3 class. This classs method does all S3 operation within the program
    """

    def __init__(self, s3_client: object, bucket: str):
        """
        Init method of S3
        :param s3_client: s3 client
        :param bucket: bucket name in S3
        """
        self.s3_client = s3_client
        self.bucket = bucket

    def list_object(self, prefix: str, fileextension: str) -> list:
        """
        This function parse the folder (passed as prefix) in the S3 bucket
        and stores the filepath in a list which matches the file extension pattern
        :param prefix: Path in bucket where to list the objects
        :param fileextension: File extension for identifying inscope file
        :return file_list: A list which contains all file path which matches the pattern
        in the S3 bucket and path.
        """
        try:
            pagination = self.s3_client.get_paginator('list_objects_v2')
            page_iterator = pagination.paginate(Bucket=self.bucket, Prefix=prefix)
            file_list = [item['Key'] for page in page_iterator
                         for item in page['Contents']
                         if item['Key'].lower().endswith(fileextension)]
            logging.info(f'List present in the s3 folder are: {file_list}')
        except Exception as ex:
            raise S3Error(f'Failed to parse the S3 bucket {self.bucket} '
                          f'and prefix {prefix} Error:{str(ex)}')
        return file_list

    def read(self, key: str):
        """
        This functions reads the content of a file and returns a list of the lines
        of the file.
        :param key: Path of the file in the bucket
        :return: a list of the lines in the file
        """
        try:
            obj = self.s3_client.get_object(Bucket=self.bucket, Key=key)
        except Exception as ex:
            raise S3Error(f'Failed to read source file {key} Error: {str(ex)}')
        return obj['Body'].read().decode('utf-8').splitlines(True)

    def write(self, body: bytes, key: str):
        """
        This function writes the content of the transformed json into the
        out path of the bucket
        :param body: JSON string generated after the transformation to be written into the file
        :param key: File path in which to write the JSON data
        """
        try:
            self.s3_client.put_object(Body=body, Bucket=self.bucket, Key=key)
            logging.info(f'Output File is generated as key: {key} in bucket {self.bucket}')
        except Exception as ex:
            raise S3Error(f'Failed to write to S3 {key} Error: {str(ex)}')

    def move(self, source_key: str, target_key: str):
        """
        This function moves the file within the bucket
        :param source_key: Source path of the file
        :param target_key: Target path of the file
        """
        try:
            self.s3_client.copy_object(Bucket=self.bucket,
                                       CopySource={'Bucket': self.bucket, 'Key': source_key},
                                       Key=target_key
                                       )
            self.s3_client.delete_object(Bucket=self.bucket, Key=source_key)
            logging.info(f'File moved from source: {source_key} to target: {target_key}')
        except Exception as ex:
            raise S3Error(f'Failed to move s3 file from {source_key}'
                          f' to  {target_key} Error: {str(ex)}')
