import boto3
import os
import pytest
from io import BytesIO
from moto import mock_s3

from src.storageutil import S3


class TestS3:

    @mock_s3
    def test_create_bucket(self):
        """Creates bucket in mocked s3 env"""
        s3_client = boto3.client('s3')
        bucket = 'mdev.tech-test.test.csvtojsontransformer.eeeeeeeeee'
        s3_client.create_bucket(Bucket=bucket)
        return bucket

    @mock_s3
    def test_list_object(self):
        s3_client = boto3.client('s3')
        bucket = self.test_create_bucket()
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data.csv')
        with open(file_path, 'rb') as f:
            response = s3_client.upload_fileobj(f, bucket, 'source/data.csv')
        file_list = S3(s3_client, bucket).list_object('source/', '.csv')
        assert file_list[0] == 'source/data.csv'

    @mock_s3
    def test_read(self):
        s3_client = boto3.client('s3')
        bucket = self.test_create_bucket()
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data.csv')
        with open(file_path, 'rb') as f:
            response = s3_client.upload_fileobj(f, bucket, 'source/data.csv')
        data_content = S3(s3_client, bucket).read('source/data.csv')
        assert data_content[1].replace('\r', '').replace('\n', '') == 'https://groceries.morrisons.com/browse,THE BEST,178974,https://groceries.morrisons.com/browse/178974,,,,,,'

    @mock_s3
    def test_write(self):
        s3_client = boto3.client('s3')
        bucket = self.test_create_bucket()
        buffer = BytesIO()
        buffer.write(b'[{"label": "THE BEST", "id": "178974", "link": "https://groceries.morrisons.com/browse/178974", "children": []}]')
        S3(s3_client, bucket).write(buffer.getvalue(), 'target/data.json')
        file_list = S3(s3_client, bucket).list_object('target/', '.json')
        assert file_list[0] == 'target/data.json'

    @mock_s3
    def test_move(self):
        s3_client = boto3.client('s3')
        bucket = self.test_create_bucket()
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data.csv')
        with open(file_path, 'rb') as f:
            response = s3_client.upload_fileobj(f, bucket, 'source/data.csv')
        S3(s3_client, bucket).move('source/data.csv', 'target/data.csv')
        file_list_target = S3(s3_client, bucket).list_object('target/', '.csv')
        assert file_list_target[0] == 'target/data.csv'

    @mock_s3
    def test_move_key_error(self):
        s3_client = boto3.client('s3')
        bucket = self.test_create_bucket()
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data.csv')
        with open(file_path, 'rb') as f:
            response = s3_client.upload_fileobj(f, bucket, 'source/data.csv')
        with pytest.raises(Exception) as e:
            S3(s3_client, bucket).move('source/data1.csv', 'target/data.csv')
        with pytest.raises(Exception) as e:
            file_list_target = S3(s3_client, bucket).list_object('target/', '.csv')

    @mock_s3
    def test_read_error(self):
        s3_client = boto3.client('s3')
        bucket = self.test_create_bucket()
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data.csv')
        with open(file_path, 'rb') as f:
            response = s3_client.upload_fileobj(f, bucket, 'source/data.csv')
        with pytest.raises(Exception) as e:
            data_content = S3(s3_client, bucket).read('source/data1.csv')
