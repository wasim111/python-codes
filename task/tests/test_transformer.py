import os
import json

from src.transformer import Transformer
from src.validate import calculate_levels
from src.csvutil import CsvReader
from src.group import GroupByLevels


class Test_Transformer:

    def test_group_unique_rows(self):
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data.csv')
        with open(file_path) as f:
            lines = f.read().splitlines(True)
        lines_list = CsvReader(lines).read_csv()
        number_of_levels = calculate_levels(lines_list)
        unique_dict = dict()
        for level in range(1, number_of_levels + 1):
            unique_dict['unique' + f'{level}'] = GroupByLevels(level).unique_rows(lines_list)
        transform = Transformer(level)
        children_dict = transform.map_children(unique_dict)
        assert children_dict['unique1'][0] == [0, 1, 2, 3, 4]

    def test_transform_to_json(self):
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data.csv')
        with open(file_path) as f:
            lines = f.read().splitlines(True)
        lines_list = CsvReader(lines).read_csv()
        number_of_levels = calculate_levels(lines_list)
        unique_dict = dict()
        for level in range(1, number_of_levels + 1):
            unique_dict['unique' + f'{level}'] = GroupByLevels(level).unique_rows(lines_list)
        transform = Transformer(level)
        children_dict = transform.map_children(unique_dict)
        json_output = transform.transform_to_json(unique_dict, children_dict)
        dict_content = json.loads(json_output)
        assert dict_content[0]['label'] == 'THE BEST'
        assert dict_content[0]['id'] == '178974'

    def test_transform_to_json_level2(self):
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data_level2.csv')
        with open(file_path) as f:
            lines = f.read().splitlines(True)
        lines_list = CsvReader(lines).read_csv()
        number_of_levels = calculate_levels(lines_list)
        unique_dict = dict()
        for level in range(1, number_of_levels + 1):
            unique_dict['unique' + f'{level}'] = GroupByLevels(level).unique_rows(lines_list)
        transform = Transformer(level)
        children_dict = transform.map_children(unique_dict)
        json_output = transform.transform_to_json(unique_dict, children_dict)
        dict_content = json.loads(json_output)
        assert number_of_levels == 2
        assert dict_content[0]['children'][0]['label'] == 'FRESH'
        assert dict_content[0]['children'][0]['id'] == '178969'
        assert dict_content[0]['children'][0]['link'] == 'https://groceries.morrisons.com/browse/178974/178969'

    def test_transform_to_json_level4(self):
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data_level4.csv')
        with open(file_path) as f:
            lines = f.read().splitlines(True)
        lines_list = CsvReader(lines).read_csv()
        number_of_levels = calculate_levels(lines_list)
        unique_dict = dict()
        for level in range(1, number_of_levels + 1):
            unique_dict['unique' + f'{level}'] = GroupByLevels(level).unique_rows(lines_list)
        transform = Transformer(level)
        children_dict = transform.map_children(unique_dict)
        json_output = transform.transform_to_json(unique_dict, children_dict)
        dict_content = json.loads(json_output)
        assert number_of_levels == 4
        assert dict_content[0]['children'][1]['children'][0]['children'][0]['label'] == 'BREAD'
        assert dict_content[0]['children'][1]['children'][0]['children'][0]['id'] == '179078'
        assert dict_content[0]['children'][1]['children'][0]['children'][0]['link'] == 'https://groceries.morrisons.com/browse/178974/178971/179023/179078'