import os

from src.group import GroupByLevels
from src.csvutil import CsvReader
from src.validate import calculate_levels


class Test_GroupByLevels:

    def test_fields_in_group(self):
        fields = GroupByLevels(2).fields_in_group()
        assert fields == 'r[0], r[1], r[2], r[3], r[4], r[5], r[6]'

    def test_compare_expression(self):
        expression = GroupByLevels(2).compare_expression()
        assert expression == 'row1[1] == row2[1] and  row1[2] == row2[2] and  row1[3] == row2[3] and  row1[4] == row2[4] and  row1[5] == row2[5] and  row1[6] == row2[6]'

    def test_unique_rows(self):
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data.csv')
        with open(file_path) as f:
            lines = f.read().splitlines(True)
        line_list = CsvReader(lines).read_csv()
        number_of_levels = calculate_levels(line_list)
        unique_rows_list = GroupByLevels(number_of_levels).unique_rows(line_list)
        assert unique_rows_list[0][1] == 'THE BEST'
        assert unique_rows_list[0][2]  == '178974'
