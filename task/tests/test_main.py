import boto3
import os
from moto import mock_s3

from src.main import main
from tests.test_config import Test_Config
from tests.test_storageutil import TestS3


@mock_s3
def test_main():
    config = Test_Config()
    s3_client = boto3.client('s3')
    bucket = TestS3().test_create_bucket()
    file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data.csv')
    with open(file_path, 'rb') as f:
        response = s3_client.upload_fileobj(f, bucket, 'source/data.csv')
    main(config, s3_client)


@mock_s3
def test_main_file_not_exist():
    config = Test_Config()
    s3_client = boto3.client('s3')
    bucket = TestS3().test_create_bucket()
    file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'test_config.py')
    with open(file_path, 'rb') as f:
        response = s3_client.upload_fileobj(f, bucket, 'source/test_config.py')
    main(config, s3_client)
