import os
from src.csvutil import CsvReader


class Test_CsvReader:

    def test_read_csv(self):
        file_path = os.path.join(os.path.join(os.getcwd(), 'tests'), 'data.csv')
        with open(file_path) as f:
            lines = f.read().splitlines(True)
        dict_content = CsvReader(lines).read_csv()
        assert dict_content[0][1] == 'THE BEST'
        assert dict_content[0][2] == '178974'