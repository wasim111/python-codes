

class Test_Config:
    def __init__(self):
        self.sourcefolder = 'source/'
        self.targetfolder = 'target/'
        self.errorfolder = 'error/'
        self.archivefolder = 'archive/'
        self.infileextension = '.csv'
        self.outfileextension = '.json'
        self.s3bucket = 'mdev.tech-test.test.csvtojsontransformer.eeeeeeeeee'
