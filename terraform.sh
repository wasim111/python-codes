ENV=$1

echo "yes"|terraform init -backend-config=../config/$(ENV).backend.config;

if terraform workspace list | grep tcs-interview-answer-$(ENV)-$(basename `pwd`); 
then terraform workspace select tcs-interview-answer-$(ENV)-$(basename `pwd`); 
else terraform workspace new tcs-interview-answer-$(ENV)-$(basename `pwd`); 
fi

terraform apply -var-file=../vars/$(ENV).tfvars.json -auto-approve;
