pipeline {

    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3'))
    }

    agent {
        node {
            label 'test-solution'
        }
    }

    stages {
        stage('Unit and Integration Test') {
            steps {
                withCredentials([[$class: 'AmazonWebServicesCredentialsBinding',
                 accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'JENKINS_TEST_SOLUTION',
                 secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
                    withDockerContainer(image: '476435990872.dkr.ecr.eu-west-1.amazonaws.com/python-3.7:latest') {
                        script {
                            sh 'virtualenv venv && source venv/bin/activate && cd task && pip install -r test_requirements.txt && pytest --cov=src tests/'
                        }
                    }
                }
            }
        }
        stage('Deploy:dev') {
            environment{
                        DOMAIN='test'
                        SERVICE_NAME='csvtojsontransformer'
            }
            when {
                branch 'develop'
            }

            steps {
                withCredentials([[$class: 'AmazonWebServicesCredentialsBinding',
                accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'JENKINS_TEST_SOLUTION',
                secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
                    sh '$(aws ecr get-login --region eu-west-1 --no-include-email )'
                    script {

					   dir("envsetup/terraform/ecsrepo") {
	    			       sh '. ../../../terraform.sh dev'
                        }
						
					    sh 'docker build -t 476435990872.dkr.ecr.eu-west-1.amazonaws.com/${DOMAIN}/${SERVICE_NAME}:dev -f envsetup/docker/Dockerfile .'
					    sh 'docker push 476435990872.dkr.ecr.eu-west-1.amazonaws.com/${DOMAIN}/${SERVICE_NAME}:dev'
						
				        dir("envsetup/terraform/ecs") {						
						   sh '. ../../../terraform.sh dev'
                        }
					}
				}
			}
        }
		
		stage('CleanWorkspace') {
			steps {
					cleanWs()
			}
		}
    }
}
